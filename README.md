# kubernetes-argocd-self-hosting

This project is an example of a config-as-code Kubernetes project monorepo. It contains:
 - A DigitalOcean Kubernetes cluster created through Terraform
 - Terraform state stored in DigitalOcean Spaces
 - ArgoCD configured to point back at the same repo
 - Projects deployed (GitOps-style) to separate namespaces and ArgoCD AppProjects
 - GitLab CI manual steps for creating and destroying the cluster, and syncing projects

The current configuration is set up how I want it for personal projects. I use this configuration to allow me to create / destroy the whole cluster in one* (or so) click so I can use a reasonably-sized cluster for just a few minutes or hours a day. It would need a different configuration for production use.

### Setup & Configuration
1. You'll need a DigitalOcean account with billing enabled, Kubernetes limited access enabled, and spaces.
2. Create an API Token for DigitalOcean (for creating the Kubernetes cluster)
3. Create a Space in DigitalOcean called `sandbox-state` (update backend.tf to change it)
4. Generate an Access Key and Secret Key for the Space
5. Fork this repo
6. Create a GitLab Deploy Token
7. Set the following GitLab CI secrets:
 - TF_VAR_do_token: Your DigitalOcean API Token
 - SPACES_ACCESS_KEY
 - SPACES_SECRET_KEY
 - REPO_URL: https:// URL for cloning the Git repo (e.g. https://gitlab.com/njea/kubernetes-argocd-self-hosting.git )
 - REPO_USERNAME: Deploy token username
 - REPO_PASSWORD: Deploy token

### Next Steps
- Use the Manual CI jobs to create / destroy the cluster
- Use the DigitalOcean UI or `terraform output kubernetes_config` to access the cluster admin kubeconfig file
- Access the ArgoCD UI through `kubectl port-forward` (or deploy an ingress)
- Log in to ArgoCD UI with u: `admin` p: `[name of the argocd server pod]` e.g. `argocd-server-67cb5c4f79-2kmfx`
