terraform {
  backend "s3" {
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    access_key                  = ""
    secret_key                  = ""
    endpoint                    = "sfo2.digitaloceanspaces.com"
    region                      = "us-east-1"                                   # Requires any valid AWS region
    bucket                      = "sandbox-state"                               # Space name
    key                         = "terraform/cluster-sandbox/terraform.tfstate"
  }
}
