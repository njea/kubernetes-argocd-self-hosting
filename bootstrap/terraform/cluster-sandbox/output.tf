output "kubernetes_config" {
  value     = "${digitalocean_kubernetes_cluster.sandbox.kube_config.0.raw_config}"
  sensitive = true
}
