resource "digitalocean_kubernetes_cluster" "sandbox" {
  name    = "sandbox"
  region  = "sfo2"
  version = "1.13.5-do.1"
  tags    = ["sandbox"]

  node_pool {
    name       = "default-worker-pool"
    size       = "s-2vcpu-2gb"
    node_count = 3
  }
}
