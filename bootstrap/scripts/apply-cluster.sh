#!/bin/sh

REPO_ROOT=$(git rev-parse --show-toplevel)
cd "${REPO_ROOT}/bootstrap/terraform/cluster-sandbox"

terraform init -backend-config="access_key=${SPACES_ACCESS_KEY}" -backend-config="secret_key=${SPACES_SECRET_KEY}"
terraform apply -auto-approve
