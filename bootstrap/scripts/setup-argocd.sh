#!/bin/sh

KUBECTL_VERSION=v1.13.5

REPO_ROOT=$(git rev-parse --show-toplevel)

setup_kubectl() {
    cd "${REPO_ROOT}/bootstrap/terraform/cluster-sandbox"

    export KUBECONFIG=/tmp/kubeconfig
    terraform init -backend-config="access_key=${TF_VAR_access_key}" -backend-config="secret_key=${TF_VAR_secret_key}"
    # Currently, terraform refresh outputs sensitive values to STDOUT: https://github.com/hashicorp/terraform/issues/19322
    terraform refresh 1>/dev/null
    terraform output kubernetes_config > "${KUBECONFIG}"

    # Download Kubectl
    curl -LO "https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl"
    chmod +x kubectl
    mv kubectl /usr/bin/kubectl
}

setup_argocd() {
    # Install argocd
    kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
}

setup_repo() {
    cat << REPO | kubectl apply -n argocd -f -
apiVersion: v1
kind: Secret
metadata:
  name: repo-credentials
  namespace: argocd
data:
  username: $(echo -n "${REPO_USERNAME}" | base64)
  password: $(echo -n "${REPO_PASSWORD}" | base64)
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: argocd-cm
  namespace: argocd
data:
  repositories: |
    - url: ${REPO_URL}
      passwordSecret:
        name: repo-credentials
        key: password
      usernameSecret:
        name: repo-credentials
        key: username
REPO
}

setup_kubectl
setup_argocd
setup_repo
