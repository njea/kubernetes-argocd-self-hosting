#!/bin/sh

KUBECTL_VERSION=v1.13.5

REPO_ROOT=$(git rev-parse --show-toplevel)

setup_kubectl() {
    cd "${REPO_ROOT}/bootstrap/terraform/cluster-sandbox"

    export KUBECONFIG=/tmp/kubeconfig
    terraform init -backend-config="access_key=${SPACES_ACCESS_KEY}" -backend-config="secret_key=${SPACES_SECRET_KEY}"
    # Currently, terraform refresh outputs sensitive values to STDOUT: https://github.com/hashicorp/terraform/issues/19322
    terraform refresh 1>/dev/null
    terraform output kubernetes_config > "${KUBECONFIG}"

    # Download Kubectl
    curl -LO "https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl"
    chmod +x kubectl
    mv kubectl /usr/bin/kubectl
}

init_projects() {
    cd "${REPO_ROOT}"

    for PROJECT_NAME in $(find projects -maxdepth 1 -mindepth 1 -type d -exec basename {} \;); do
        case "${PROJECT_NAME}" in
            kube-system|default)
                continue
                ;;
            *)
                ;;
        esac

        cat << PRJ | kubectl apply -f -
apiVersion: v1
kind: Namespace
metadata:
  creationTimestamp: null
  name: ${PROJECT_NAME}
---
apiVersion: argoproj.io/v1alpha1
kind: AppProject
metadata:
  name: ${PROJECT_NAME}
  namespace: argocd
spec:
  description: ${PROJECT_NAME}
  sourceRepos:
  - ${REPO_URL}
  destinations:
  - namespace: ${PROJECT_NAME}
    server: "https://kubernetes.default.svc"
  clusterResourceWhitelist: []
  namespaceResourceBlacklist:
  - group: ''
    kind: ResourceQuota
  - group: ''
    kind: LimitRange
  - group: ''
    kind: NetworkPolicy
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: ${PROJECT_NAME}
  namespace: argocd
spec:
  destination:
    namespace: ${PROJECT_NAME}
    server: 'https://kubernetes.default.svc'
  source:
    path: projects/${PROJECT_NAME}
    repoURL: ${REPO_URL}
    targetRevision: HEAD
    directory:
      recurse: true
  project: ${PROJECT_NAME}
  syncPolicy:
    automated:
      prune: true
PRJ
    done
}

setup_kubectl
init_projects
